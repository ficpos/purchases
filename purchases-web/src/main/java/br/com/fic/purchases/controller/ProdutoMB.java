package br.com.fic.purchases.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fic.purchases.business.FornecedorBusinessLocal;
import br.com.fic.purchases.business.ProdutoFornecedorBusinessLocal;
import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

@ManagedBean
@ConversationScoped
public class ProdutoMB implements Serializable {
	private static final long serialVersionUID = -7802073407907474795L;

	private ProdutoFornecedor produtoFornecedorSelecionado = new ProdutoFornecedor();
	private Fornecedor fornecedorSelecionado = new Fornecedor();
	private Produto produtoSelecionado = new Produto();

	@EJB
	private FornecedorBusinessLocal fornecedorBusiness;

	@EJB
	private ProdutoFornecedorBusinessLocal produtoFornecedorBusiness;
	

	public void salvar() {
		this.produtoFornecedorSelecionado.setProduto(produtoSelecionado);
		produtoFornecedorBusiness.salvarProdutoFornecedor(produtoFornecedorSelecionado, fornecedorSelecionado);
		this.produtoSelecionado = new Produto();
		this.fornecedorSelecionado = new Fornecedor();
		this.produtoFornecedorSelecionado = new ProdutoFornecedor();
	}

	
	public List<Fornecedor> getFornecedores() {
		return fornecedorBusiness.selecionarFornecedores();
	}
	
	public List<ProdutoFornecedor> getProdutosFornecedores() {
		return produtoFornecedorBusiness.selecionarProdutosFornecedores();
	}
	
	
	//GETS and SETS
	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;
	}
	
	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}


	public ProdutoFornecedor getProdutoFornecedorSelecionado() {
		return produtoFornecedorSelecionado;
	}
	
	public void setProdutoFornecedorSelecionado(ProdutoFornecedor produtoFornecedorSelecionado) {
		this.produtoFornecedorSelecionado = produtoFornecedorSelecionado;
	}

}
