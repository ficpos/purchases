package br.com.fic.purchases.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fic.purchases.business.FornecedorBusinessLocal;
import br.com.fic.purchases.model.Fornecedor;

@ManagedBean
@ConversationScoped
public class FornecedorMB implements Serializable{

	private static final long serialVersionUID = 6340580511975270569L;

	private Fornecedor fornecedorSelecionado = new Fornecedor();
	
	@EJB(name="FornecedorBusiness")
	private FornecedorBusinessLocal fornecedorBusiness;
	
	public void salvar() {
		fornecedorBusiness.salvarFornecedor(fornecedorSelecionado);
		this.fornecedorSelecionado = new Fornecedor();
	}
	
	public List<Fornecedor> getFornecedores() {
		return fornecedorBusiness.selecionarFornecedores();
	}
	

	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;
	}

	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;
	}
	
	
}
