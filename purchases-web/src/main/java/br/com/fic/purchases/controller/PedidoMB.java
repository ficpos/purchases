package br.com.fic.purchases.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import br.com.fic.purchases.business.FornecedorBusinessLocal;
import br.com.fic.purchases.business.ProdutoFornecedorBusinessLocal;
import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.ItemPedido;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

@ManagedBean
@ViewScoped
public class PedidoMB implements Serializable {
	private static final long serialVersionUID = -4704559401429631194L;

	private ProdutoFornecedor produtoFornecedorSelecionado = new ProdutoFornecedor();
	private Fornecedor fornecedorSelecionado = new Fornecedor();
	private List<ProdutoFornecedor> produtoPorFornecedor = new ArrayList<ProdutoFornecedor>();
	private BigDecimal precoProdudo;
	

	private ItemPedido item = new ItemPedido();
	private List<ItemPedido> itens = new ArrayList<ItemPedido>();

	@EJB
	private FornecedorBusinessLocal fornecedorBusiness;
	@EJB
	private ProdutoFornecedorBusinessLocal produtoFornecedorBusiness;

	public void adcionarItem() {
		this.itens.add(item);
	}

	public void loadProdutoPorFornecedor(ValueChangeEvent evt) {
		System.out.println("Loading...");
		if (evt.getNewValue() != null) {
			Fornecedor consultaFornecedor = new Fornecedor();
			consultaFornecedor.setId(Long.valueOf(evt.getNewValue().toString()));
			this.produtoPorFornecedor = produtoFornecedorBusiness.selecionarProdutoPorFornecedor(consultaFornecedor);
		} else {
			System.out.println("Iniciando a lista de produtos");
			this.produtoPorFornecedor = new ArrayList<ProdutoFornecedor>();
		}
		System.out.println("Iniciando o preço do produto");
		this.precoProdudo = null;
		
	}
	
	public void loadPrecoProduto(ValueChangeEvent evt) {
		System.out.println("Carregando Preço");
		if(evt.getNewValue() != null){
			Produto produto = new Produto();
			produto.setId(Long.valueOf(evt.getNewValue().toString()));
			this.precoProdudo = produtoFornecedorBusiness.carregarPrecoProduto(produto);
			
		}
	}

	// GETS AND SETS
	public ProdutoFornecedor getProdutoFornecedorSelecionado() {
		return produtoFornecedorSelecionado;
	}

	public void setProdutoFornecedorSelecionado(
			ProdutoFornecedor produtoFornecedorSelecionado) {
		this.produtoFornecedorSelecionado = produtoFornecedorSelecionado;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedorBusiness.selecionarFornecedores();
	}

	public List<ProdutoFornecedor> getProdutoPorFornecedor() {
		return produtoPorFornecedor;
	}

	public void setProdutoPorFornecedor(
			List<ProdutoFornecedor> produtoPorFornecedor) {
		this.produtoPorFornecedor = produtoPorFornecedor;
	}
	
	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;
	}
	
	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;
	}
	
	public BigDecimal getPrecoProdudo() {
		return precoProdudo;
	}
	
	public void setPrecoProdudo(BigDecimal precoProdudo) {
		this.precoProdudo = precoProdudo;
	}
}
