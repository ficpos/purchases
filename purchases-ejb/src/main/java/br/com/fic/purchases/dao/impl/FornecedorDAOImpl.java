package br.com.fic.purchases.dao.impl;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.fic.purchases.dao.FornecedorDAOLocal;
import br.com.fic.purchases.model.Fornecedor;

@Local(FornecedorDAOLocal.class)
@Stateless(name = "FornecedorDAO")
public class FornecedorDAOImpl extends JpaDAOGenerico<Fornecedor> implements
		FornecedorDAOLocal {

	@Override
	protected Class<Fornecedor> getClasseEntidade() {
		return Fornecedor.class;
	}

	public void salvarFornecedor(Fornecedor fornecedor) {
		super.salvarEntidade(fornecedor);
	}
	
	public Fornecedor selecionarFornecedor(Fornecedor fornecedor) {
		return super.selecionarEntidade(fornecedor);
	}
	
	public List<Fornecedor> selecionarFornecedores(){
		return super.selecionarTodos();
	}
	
	public void excluirFornecedor(Fornecedor fornecedor){
		super.excluirEntidade(fornecedor);
	}
}
