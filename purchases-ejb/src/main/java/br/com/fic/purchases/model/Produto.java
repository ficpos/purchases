package br.com.fic.purchases.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "produtos")
@AttributeOverride(name = "id", column = @Column(name = "produto_id"))
public class Produto extends BaseModel {
	private static final long serialVersionUID = 5236412410311931761L;

	private String codigo;
	private String descricao;
	private String unidadeMedida;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	@Override
	public String toString() {
		return "Produto [codigo=" + codigo + ", descrição=" + descricao
				+ ", unidadeMedida=" + unidadeMedida + "]";
	}

}
