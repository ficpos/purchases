package br.com.fic.purchases.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.fic.purchases.model.BaseModel;

public abstract class JpaDAOGenerico<T extends BaseModel> {

	@PersistenceContext(unitName="purchases-PU")
	protected EntityManager entityManager;
	
	protected abstract Class<T> getClasseEntidade();
	
	protected void inserirEntidade(T entidade) {
		entityManager.persist(entidade);
	}
	protected void excluirEntidade(T entidade){
		T entity = selecionarEntidade(entidade);
		entityManager.remove(entity);
	}
	
	protected void atualizarEntidade(T entidade) {
		entityManager.merge(entidade);
	}
	
	protected void salvarEntidade(T entidade) {
		if(entidade.getId() == null){
			inserirEntidade(entidade);
		}else {
			atualizarEntidade(entidade);
		}
	}
	
	protected List<T> selecionarTodos() {
		return entityManager.createQuery("SELECT e FROM " +
				getClasseEntidade().getName() + " e ", getClasseEntidade()).getResultList();
	
	}

	public T selecionarEntidade(T entidade) {
		return entityManager.find(getClasseEntidade(), entidade.getId());
	}
}
