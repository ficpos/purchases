package br.com.fic.purchases.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.fic.purchases.business.FornecedorBusinessLocal;
import br.com.fic.purchases.dao.FornecedorDAOLocal;
import br.com.fic.purchases.model.Fornecedor;

@Local(FornecedorBusinessLocal.class)
@Stateless(name="FornecedorBusiness")
public class FornecedorBusinessImpl implements FornecedorBusinessLocal{
	
	@EJB(name="FornecedorDAO")
	private FornecedorDAOLocal dao;
	
	@Override
	public void salvarFornecedor(Fornecedor fornecedor) {
		dao.salvarFornecedor(fornecedor);
		
	}

	@Override
	public Fornecedor selecionarFornecedor(Fornecedor fornecedor) {
		return dao.selecionarFornecedor(fornecedor);
	}

	@Override
	public List<Fornecedor> selecionarFornecedores() {
		return dao.selecionarFornecedores();
	}

	@Override
	public void excluirFornecedor(Fornecedor fornecedor) {
		dao.excluirFornecedor(fornecedor);
		
	}

}
