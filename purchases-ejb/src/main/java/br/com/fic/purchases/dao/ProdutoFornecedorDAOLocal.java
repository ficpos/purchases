package br.com.fic.purchases.dao;

import java.math.BigDecimal;
import java.util.List;

import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;


public interface ProdutoFornecedorDAOLocal {
	public void salvarProdutoFornecedor(ProdutoFornecedor produtoFonecedor);
	public List<ProdutoFornecedor> selecionarProdutosFornecedores(); 
	public List<ProdutoFornecedor> selecionarProdutoPorFornecedor(Fornecedor fornecedor);
	public BigDecimal carregarPrecoProduto(Produto produto);
	
}
