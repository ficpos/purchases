package br.com.fic.purchases.business;

import java.math.BigDecimal;
import java.util.List;

import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

public interface ProdutoFornecedorBusinessLocal {
	public void salvarProdutoFornecedor(ProdutoFornecedor produtoFornecedor,
			Fornecedor fornecedor);

	public List<ProdutoFornecedor> selecionarProdutosFornecedores();

	public List<ProdutoFornecedor> selecionarProdutoPorFornecedor(
			Fornecedor fornecedor);

	public BigDecimal carregarPrecoProduto(Produto produto);
}
