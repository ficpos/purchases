package br.com.fic.purchases.model;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "produtos_fornecedores")
@AttributeOverride(name = "id", column = @Column(name = "produto_fornecedor_id"))
public class ProdutoFornecedor extends BaseModel {
	private static final long serialVersionUID = 278447755013503092L;
	
	@Column(name="preco")
	private BigDecimal precoUnitario;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "produto_id")
	private Produto produto;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fornecedor_id")
	private Fornecedor fornecedor;

	public BigDecimal getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(BigDecimal precoUnitario) {
		this.precoUnitario = precoUnitario;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public String toString() {
		return "ProdutoFonecedor [precoUnitario=" + precoUnitario
				+ ", produto=" + produto + ", fornecedor=" + fornecedor + "]";
	}

	
}
