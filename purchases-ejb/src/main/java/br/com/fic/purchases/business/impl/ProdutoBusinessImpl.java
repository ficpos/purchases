package br.com.fic.purchases.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.fic.purchases.business.ProdutoBusinessLocal;
import br.com.fic.purchases.dao.ProdutoDAOLocal;
import br.com.fic.purchases.model.Produto;

@Local(ProdutoBusinessLocal.class)
@Stateless(name = "ProdutoBusiness")
public class ProdutoBusinessImpl implements ProdutoBusinessLocal {

	@EJB
	private ProdutoDAOLocal dao;

	@Override
	public void salvarProduto(Produto produto) {
		dao.salvarProduto(produto);
	}

	@Override
	public Produto selecionarProduto(Produto produto) {
		return dao.selecionarProduto(produto);
	}

	@Override
	public List<Produto> selecionarProdutos() {
		return dao.selecionarProdutos();
	}

	@Override
	public void excluirProduto(Produto produto) {
		dao.excluirProduto(produto);

	}

}
