package br.com.fic.purchases.dao;

import java.util.List;

import br.com.fic.purchases.model.Produto;


public interface ProdutoDAOLocal {
	public void salvarProduto(Produto produto);

	public Produto selecionarProduto(Produto produto);

	public List<Produto> selecionarProdutos();

	public void excluirProduto(Produto produto);
}
