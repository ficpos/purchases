package br.com.fic.purchases.dao.impl;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.fic.purchases.dao.ProdutoDAOLocal;
import br.com.fic.purchases.model.Produto;

@Local(ProdutoDAOLocal.class)
@Stateless(name = "ProdutoDAO")
public class ProdutoDAOImpl extends JpaDAOGenerico<Produto> implements
		ProdutoDAOLocal {

	@Override
	protected Class<Produto> getClasseEntidade() {
		return Produto.class;
	}

	@Override
	public void salvarProduto(Produto produto) {
		super.salvarEntidade(produto);
	}

	@Override
	public Produto selecionarProduto(Produto produto) {
		return super.selecionarEntidade(produto);
	}

	@Override
	public List<Produto> selecionarProdutos() {
		return super.selecionarTodos();
	}

	@Override
	public void excluirProduto(Produto produto) {
		super.excluirEntidade(produto);

	}

}
