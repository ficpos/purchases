package br.com.fic.purchases.util;

import java.math.BigDecimal;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

public class DatabaseGen {
	public static void main(String[] args) {
		 final Properties config = new Properties(); 
		 config.put("hibernate.hbm2ddl.auto", "create");
		 
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("purchases-PU", config);
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
//		Produto p1 = new Produto();
//		p1.setCodigo("789452734522");
//		p1.setDescricao("Feijão Tio João");
//		p1.setUnidadeMedida("KG");
//		
//		Produto p2 = new Produto();
//		p2.setCodigo("7894758742212");
//		p2.setDescricao("Arroz Alteza");
//		p2.setUnidadeMedida("KG");
//		
//		Fornecedor f1 =  new Fornecedor();
//		f1.setNome("Carrefu");
//		f1.setEmail("carrefu@carrefu.com.br");
//		
//		ProdutoFornecedor pf1 = new ProdutoFornecedor();
//		pf1.setFornecedor(f1);
//		pf1.setProduto(p1);
//		pf1.setPrecoUnitario(new BigDecimal(5.30));
//		
//		ProdutoFornecedor pf2 = new ProdutoFornecedor();
//		pf2.setFornecedor(f1);
//		pf2.setProduto(p2);
//		pf2.setPrecoUnitario(new BigDecimal(3.50));
//		
//		em.persist(pf1);
//		em.persist(pf2);
//		
//		em.getTransaction().commit();
		em.close();
	}

}
