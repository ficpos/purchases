package br.com.fic.purchases.model;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="itens_pedido")
@AttributeOverride(name = "id", column = @Column(name = "item_id"))
public class ItemPedido extends BaseModel {
	private static final long serialVersionUID = -6974561161425067959L;
	
	private Integer quantidade;
	
	@ManyToOne
	@JoinColumn(name = "pedido_id")
	private Pedido pedido;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="produto_fornecedor_id")
	private ProdutoFornecedor produtoFornecedor;

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public ProdutoFornecedor getProdutoFornecedor() {
		return produtoFornecedor;
	}

	public void setProdutoFornecedor(ProdutoFornecedor produtoFornecedor) {
		this.produtoFornecedor = produtoFornecedor;
	}

	@Override
	public String toString() {
		return "ItemPedido [quantidade=" + quantidade + ", pedido=" + pedido + ", produtoFornecedor=" + produtoFornecedor + "]";
	}
	
	
}

