package br.com.fic.purchases.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "fornecedores")
@AttributeOverride(name = "id", column = @Column(name = "fornecedor_Id"))
public class Fornecedor extends BaseModel {
	private static final long serialVersionUID = 3147680336290724374L;

	private String nome;
	private String email;

	@OneToMany(mappedBy = "fornecedor")
	private List<ProdutoFornecedor> produtos = new ArrayList<ProdutoFornecedor>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ProdutoFornecedor> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoFornecedor> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "Fornecedor [nome=" + nome + ", email=" + email + ", produtos="
				+ produtos + "]";
	}

}
