package br.com.fic.purchases.business.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.fic.purchases.business.ProdutoFornecedorBusinessLocal;
import br.com.fic.purchases.dao.FornecedorDAOLocal;
import br.com.fic.purchases.dao.ProdutoFornecedorDAOLocal;
import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

@Local(ProdutoFornecedorBusinessLocal.class)
@Stateless(name = "ProdutoFornecedorBusiness")
public class ProdutoFornecedorBusinessImpl implements ProdutoFornecedorBusinessLocal {

	@EJB(name = "ProdutoFornecedorDAO")
	private ProdutoFornecedorDAOLocal dao;

	@EJB(name = "FornecedorDAO")
	private FornecedorDAOLocal daoFornecedor;

	@Override
	public void salvarProdutoFornecedor(ProdutoFornecedor produtoFornecedor, Fornecedor fornecedor) {
		Fornecedor fornecedorConsultado = daoFornecedor.selecionarFornecedor(fornecedor);
		produtoFornecedor.setFornecedor(fornecedorConsultado);
		dao.salvarProdutoFornecedor(produtoFornecedor);

	}

	@Override
	public List<ProdutoFornecedor> selecionarProdutosFornecedores() {
		return dao.selecionarProdutosFornecedores();
	}

	@Override
	public List<ProdutoFornecedor> selecionarProdutoPorFornecedor(
			Fornecedor fornecedor) {
		return dao.selecionarProdutoPorFornecedor(fornecedor);
	}

	@Override
	public BigDecimal carregarPrecoProduto(Produto produto) {
		return dao.carregarPrecoProduto(produto);
	}


}
