package br.com.fic.purchases.business;

import java.util.List;

import br.com.fic.purchases.model.Fornecedor;

public interface FornecedorBusinessLocal {

	void salvarFornecedor(Fornecedor fornecedor);
	Fornecedor selecionarFornecedor(Fornecedor fornecedor);
	List<Fornecedor> selecionarFornecedores();
	void excluirFornecedor(Fornecedor fornecedor);
}
