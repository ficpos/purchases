package br.com.fic.purchases.business;

import java.util.List;

import br.com.fic.purchases.model.Produto;

public interface ProdutoBusinessLocal {
	public void salvarProduto(Produto produto);

	public Produto selecionarProduto(Produto produto);

	public List<Produto> selecionarProdutos();

	public void excluirProduto(Produto produto);
}
