package br.com.fic.purchases.dao;

import java.util.List;

import br.com.fic.purchases.model.Fornecedor;

public interface FornecedorDAOLocal {
	void salvarFornecedor(Fornecedor fornecedor);
	Fornecedor selecionarFornecedor(Fornecedor fornecedor);
	List<Fornecedor> selecionarFornecedores();
	void excluirFornecedor(Fornecedor fornecedor);
}
