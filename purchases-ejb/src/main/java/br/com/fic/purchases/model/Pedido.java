package br.com.fic.purchases.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import br.com.fic.purchases.util.Status;

@Entity
@AttributeOverride(name="id", column= @Column(name="pedido_id"))
public class Pedido extends BaseModel {

	private static final long serialVersionUID = 7610504440350887605L;

	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany(mappedBy="pedido")
	private List<ItemPedido> itens = new ArrayList<ItemPedido>();

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		return "Pedido [status=" + status + ", itens=" + itens + "]";
	}
	
	
	
}
