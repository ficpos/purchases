package br.com.fic.purchases.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.fic.purchases.dao.ProdutoFornecedorDAOLocal;
import br.com.fic.purchases.model.Fornecedor;
import br.com.fic.purchases.model.Produto;
import br.com.fic.purchases.model.ProdutoFornecedor;

@Local(ProdutoFornecedorDAOLocal.class)
@Stateless(name="ProdutoFornecedorDAO")
public class ProdutoFornecedorDAOImpl extends JpaDAOGenerico<ProdutoFornecedor>
		implements ProdutoFornecedorDAOLocal {

	@Override
	public void salvarProdutoFornecedor(ProdutoFornecedor produtoFonecedor) {
		super.salvarEntidade(produtoFonecedor);
		
	}

	@Override
	protected Class<ProdutoFornecedor> getClasseEntidade() {
		return ProdutoFornecedor.class;
	}

	@Override
	public List<ProdutoFornecedor> selecionarProdutosFornecedores() {
		List<ProdutoFornecedor> lista = new ArrayList<ProdutoFornecedor>();
		String sql = "SELECT pf FROM ProdutoFornecedor pf join fetch pf.fornecedor join fetch pf.produto";
		lista = entityManager.createQuery(sql, ProdutoFornecedor.class).getResultList();
		
		return lista;
				
	}

	@Override
	public List<ProdutoFornecedor> selecionarProdutoPorFornecedor(
			Fornecedor fornecedor) {
		List<ProdutoFornecedor> lista = new ArrayList<ProdutoFornecedor>();
		TypedQuery<ProdutoFornecedor> query = entityManager.createQuery("SELECT pf FROM ProdutoFornecedor pf"
				+ "  WHERE pf.fornecedor.id = :id", ProdutoFornecedor.class);
		
		lista =  query.setParameter("id", fornecedor.getId()).getResultList();
		
		return lista;
	}

	@Override
	public BigDecimal carregarPrecoProduto(Produto produto) {
		ProdutoFornecedor produtoFornecedor = new ProdutoFornecedor();
		TypedQuery<ProdutoFornecedor> query = entityManager.createQuery("SELECT pf FROM ProdutoFornecedor pf"
				+ " WHERE pf.produto.id = :id", ProdutoFornecedor.class);
		
		produtoFornecedor = query.setParameter("id", produto.getId()).getSingleResult();

		return produtoFornecedor.getPrecoUnitario();
	}

}
